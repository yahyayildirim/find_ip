# find_ip
Bu Script ile Local ağımızda bizimle aynı İP aralığında olan adresleri listeleyebiliriz.
Script otomatik olarak İp adresimizi bularak listeleme yapacaktır, şayet farklı bir İp bloğunda listeleme yapmak isterseniz find_ip.sh dosyasında 3. yolu manul düzenleyebilirsiniz.


# Kullanım:
> Uçbirimi açın ve aşağıdaki kodları sırayla çalıştırın.
>- `wget -c O- https://gitlab.com/yahyayildirim/find_ip/-/raw/main/find_ip.sh`
>- `chmod +x find_ip.sh`
>- `./find_ip.sh`
>* Sizden yönetici parolası isteyecektir. Parola girdiğinizde ekranda hernagi bir şey görünmeyeceği için siz şifrenizi doğru bir şekilde girin ve ENTER tuşuna basın.
